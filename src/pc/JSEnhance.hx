package pc;

import js.html.Element;
import jQuery.*;

class JSEnhance {

  // find all html tagcs which have a haxe attribute, then create the client side instance
  static public function enhance(e: Element) {
    new JQuery(e).find('[haxe]').each(function(i, e){
      var eq = new JQuery(e);
      var x:Dynamic = haxe.Unserializer.run(eq.attr("haxe"));
      var clazz = Type.resolveClass(x.class_);
      if (eq.data('haxe_instance') == null){

        if (x.args.length == 0)
          x.args.push({});
        Reflect.setField(x.args[0], 'el', eq);
        Reflect.setField(x.args[0], 'ctx', JSMain.ctx);

        if (clazz == null){
          trace('${x.class_} unkown - TODO recompile');
        } else {
          var haxei = Type.createInstance(clazz, x.args);
          eq.data('haxe_instance', haxei);
          try{
            cast(haxei, pc.PC<Dynamic>).js_setup(eq);
          }catch(e:Dynamic){
            trace("failled runnig js_setup on ", haxei);
          }
        }
      }
    });
  }

  static public function resize(e: Element) {
    new JQuery(e).find('[haxe]').each(function(i, e){
      var eq = new JQuery(e);
      var hi = eq.data('haxe_instance');
      if (hi != null){
        try{
          cast(hi, pc.PC<Dynamic>).resize();
        #if DEBUG
          trace("no resize for " +hi);
        #end
        }catch(e:Dynamic){
        }
      }
    });
  }
}
