package pc;
#if !macro

import pc.PC;
import Cfg;
import pc.form.Content;
import pc.form.layout.Helper;

#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end

#if SERVER
import web.RequestResponseHelper;
#end

using Reflect;

class Form<O:{
  ?id:String,
  ctx:Ctx
  }> extends PC<O> {

  // using anon objects so that you can pass in {foo: "abc"} easily, should be
  // typed, but is too complicated at the momernt
  // Eg in case of date the values can be either String (raw value) or date object
  public var values: Dynamic; 
  public var errors: Dynamic; // per field String or null
  public var additionalErrors: Array<String>; // eg password missmatch or such
  public var formContent(default, set_formContent): Content;
  public var setting_up:Bool;

  public function set_formContent(c:Content):Content {
    this.formContent = c;
    c.form = this;
    return c;
  }

#if JS_CLIENT
  override public function js_setup(el) {
    super.js_setup(el);
    this.formContent.js_setup(el);
    this.formContent.assignValuesFromDOM();
    values_changed();
    this.setting_up = false;
    this.el.submit(function(event){
        try{
          if (!this.beforeSubmit())
            event.preventDefault();
        }catch(e:Dynamic){
          event.preventDefault();
          throw e;
        }
    });
  }

  public function beforeSubmit(){
    check();
    if (hasErrors()){
      updateErrorBox();
      this.formContent.jsUpdateErrorDisplay();
      // show assign errors
      return false;
    } else {
      return true;
    }
  }

  public function updateErrorBox() {
    this.el.find('.errors').html(errorBoxHTMLContent());
  }

  override function resize(){
    var form_width = this.el.find('.inner_form').width();
    if (form_width > 2.3 * Cfg.label_width){
      // label | field
      this.el.find('.form_field').width(form_width - Cfg.label_width - 2);
    } else {
      // label
      // field
      this.el.find('.form_field').width(form_width -1);
    }
  }

  public function values_changed() {
#if DEBUG trace("form values changed" + values); #end
    this.formContent.values_changed();
  }

#end

  public function new(o) {
    super(o);

    this.setting_up = true;
    this.values = {};
    this.additionalErrors = [];
    this.errors = {};
  }

  public function hasErrors() {
    return Reflect.fields(this.errors).length > 0 || this.additionalErrors.length > 0;
  }


  public function errorBoxHTMLContent() {
    return mw.HTMLTemplate.str("
      %ul
        :for(n in this.errors.fields())
          %li=(n+': '+this.errors.field(n))
        :for(e in this.additionalErrors)
          %li=e
    ");
  }

  public function errorBoxHTML() {
    return mw.HTMLTemplate.str("
      .errors
        !=errorBoxHTMLContent()
    ");
  }

  public function htmlForm():String {
    return mw.HTMLTemplate.str("
      .inner_form
        !=errorBoxHTML()
        !=formContent.html()
    ");
  }

  override public function innerHTML() {
    var action = this.actionURL("form", {})+"?iframe=1";
    return mw.HTMLTemplate.str("
      %form.form(target=this.o.id+'_iframe' enctype='multipart/form-data' method='post' action=action)
        !=this.htmlForm()
      %iframe(name=o.id+'_iframe' style='height:0px; width:0px; visibility:hidden;')
    ");
  }

  public function check() {
    this.errors = {};
    this.formContent.assignErrors();
  }

#if SERVER

  public function action(): ActionResult {
    throw "override this";
  }

  public function action_form(data:Dynamic):ActionResult {
    // assigne values
    this.values = {};
    this.additionalErrors = [];
    this.errors = {};
    var ctx = this.o.ctx;
    this.formContent.assignValuesFromPost({
      post: function(s){ return web.RequestResponseHelper.postOrNull(ctx, s); },
      file: function(s){ return web.RequestResponseHelper.fileOrNull(ctx, s); }
    });
    check();

    if (hasErrors()){
      #if DEBUG trace('form has errors: ${this.errors} additionalErrors: ${this.additionalErrors}'); #end
      return ar_replace_html_of_dom_el({id: this.o.id, html: html()});
    } else {
      #if DEBUG trace("form has no errors"); #end
      try{
        return action();
      }catch(e:ActionError){
        // we have an action error, return the same form again:
        additionalErrors.push(e.msg);
        return ar_replace_html_of_dom_el({id: o.id, html: html()});
      }
    }
  }
#end
}

#end
