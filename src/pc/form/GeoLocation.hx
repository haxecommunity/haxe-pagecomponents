package pc.form;
import pc.form.Content;
import web.RequestResponseHelper;

#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end
using Reflect;

// represents a simple
typedef GeoLocationData = {
  name: String,
  label: String,
  translate: TranslationFunc,
  ?check: check.CheckFunction,
  ?html_tags: Dynamic, // { style="....", etc ... }
  ?default_: Dynamic,
  ?hint:String
};

// represents a simple input field
class GeoLocation
  implements Content {

  public var o: GeoLocationData;
  public var form(default, set_form): Form<Dynamic>;
  public function set_form(f:Form<Dynamic>){
	this.form = f;
	return f;
  }

  public function values_changed(){ };

  public function getLabel(){ return o.label; };
  public function getName(){ return o.name; };

  public function assignErrors(){
    if (o.check == null) return;
    var e = o.check(o.translate, form.values.field(o.name));
    if (e != null)
      form.errors.setField(o.name, e);
  }

  public function new(o:GeoLocationData){
	this.o = o;
    if (o.html_tags == null)
      o.html_tags = {}
  }

  public function html():String {
    var value = form.values.field(o.name);
    if (value == null) value = this.o.default_;
    var error = form.errors.field(o.name);
    return
        mw.HTMLTemplate.str('
          %input(name=this.o.name title=this.o.hint error=error this.o.html_tags value=value)
        ');
  }

#if SERVER
  public function assignValuesFromPost(avp:AVP){
     form.values.setField(o.name, avp.post(o.name));
  }
#end

#if JS_CLIENT

  public var el: JQuery;

  public function assignValuesFromDOM(){
    form.values.setField(o.name, el.val());
  }
  public function jsUpdateErrorDisplay(){
  }

  public function js_setup(form:JQuery) {
    this.el = form.find('[name=${o.name}]');
    // if no geolocation has been set try to get it from API
    trace("trying to get coordinates");
    trace(js.Browser.navigator.geolocation.getCurrentPosition(function(pos){
        trace("got coordinates");
        el.val(Location.format_lat_lon({lat: pos.coords.latitude, lon: pos.coords.longitude}));
        return null;
    }, 
      // got no position
    function(pe){
      trace("failed with "+pe.message);
      if (el.val() == ""){
        el.val(this.o.translate("your address", "geo location retrieval on js side failed, keep the translation short"));
      }
      return null;
    },
      {timeout:2000}
    ));
  }

#end
}
