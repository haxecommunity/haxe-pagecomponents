package pc.form.layout;

enum FormFieldLayout {
  horizontal_table;
  horizontal_items;
  vertical_table;
  list;
}

class Helper {
  // provide some helper functions which are used by the other code

  static public function table_row(x:Content) {
	if (x == null) throw "x is null";
    return mw.HTMLTemplate.str("
            .form_label
              %label.form_label(for=x.getName())=x.getLabel()
            .form_field
              !=x.html()
            .clear
        ");
  }

  static public function fieldsHTML(fields:Array<Content>, layout: FormFieldLayout) {
    return switch (layout) {
      case list:
        mw.HTMLTemplate.str("
          :for(x in fields)
            !=x.html()
        ");
      case horizontal_table:
        mw.HTMLTemplate.str("
            %table
              %tr
                :for(x in fields)
                  %th
                    %label.form_label(for=x.getName())=x.getLabel()
              %tr
                :for(x in fields)
                  %td
                    !=x.html()
        ");
      case vertical_table:
        mw.HTMLTemplate.str("
            %table
              :for(x in fields)
                !=table_row(x)
        ");
      case horizontal_items:
        mw.HTMLTemplate.str("
          :for(x in fields)
            %div.form_horizontal_item(style='float:left')
              %div
                %label.form_label(for=x.getName())=x.getLabel()
              %div
                !=x.html()
          %br(clear='all')
        ");
    }
  }

}
