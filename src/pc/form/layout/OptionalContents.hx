package pc.form.layout;
import pc.form.Input;
import pc.form.Content;

#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end

using Reflect;

// optional set of form elements. If option is not checked, don't assign erros/values/..
typedef OptionalContentsData = {
  active: {} -> Bool,
  ?label: String,
  group_name: String,
  formContent: Content,
  translate: TranslationFunc,
};

class OptionalContents implements Content {
  public var o: OptionalContentsData;
  public var toggler: Input;

  public var form(default, set_form): Form<Dynamic>;
  public function set_form(f:Form<Dynamic>) {
    this.form = f;
    o.formContent.form = f;
    return f;
  }

  public function getLabel() { return this.o.label; }
  public function getName() { return this.o.label; }

  public function new(o:OptionalContentsData) {
    this.o = o;
  }

  public function html():String {
	return mw.HTMLTemplate.str("
      %div(id=o.group_name)
        !=o.formContent.html()
      ");
  }

  public function assignErrors() {
	if (o.active(form.values))
	  this.o.formContent.assignErrors();
  }

#if SERVER
  public function assignValuesFromPost(avp:AVP):Void {
    var active = (o.active(form.values));
    if (active){
      this.o.formContent.assignValuesFromPost(avp);
    }
  }
#end

#if JS_CLIENT

  public var el_group: JQuery;

  public function values_changed() {
    o.formContent.values_changed();
    set_visibility();
  }

  public function active() {
    var r = o.active(form.values);
#if DEBUG
    trace("pc.form.layout.OptionalContents "+ this.o.group_name+"form values: "+form.values + " active:"+r);
#end
    return r;
  }

  public function assignValuesFromDOM(){
    if (active()){
      this.o.formContent.assignValuesFromDOM();
    }
  }

  public function jsUpdateErrorDisplay(){
    if (active())
      this.o.formContent.jsUpdateErrorDisplay();
  }
  public function js_setup(form:JQuery){
    o.formContent.js_setup(form);
    this.el_group = new JQuery('#'+ o.group_name);
    set_visibility();
  }

  public function set_visibility() {
    if (active()){
      el_group.show();
    } else {
      el_group.hide();
    }
  }

#end

}

