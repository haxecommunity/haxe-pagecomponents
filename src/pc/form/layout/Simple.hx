package pc.form.layout;
import pc.form.layout.Helper;
import pc.form.Content;

#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end


typedef SimpleData = {
  elements: Array<Content>,
  ?id: String,
  ?layout: FormFieldLayout
};
class Simple implements Content {

  public var form(default, set_form): Form<Dynamic>;

  public function set_form(f:Form<Dynamic>){
    this.form = f;
    for(e in o.elements)
      e.form = f;
    return f;
  }

  public function getLabel() { return ""; }
  public function getName() { return ""; }

  public var o: SimpleData;

  public function new(o:SimpleData) {
    this.o = o;
    if (o.layout == null)
      o.layout = vertical_table;
  }

  public function html():String {
    return Helper.fieldsHTML(o.elements, o.layout);
  }


  public function assignErrors():Void {
	for (x in o.elements) x.assignErrors();
  }

#if SERVER
  public function assignValuesFromPost(avp:AVP):Void {
    for (x in o.elements) x.assignValuesFromPost(avp);
  }
#end

#if JS_CLIENT
  public function values_changed(){
    for(e in o.elements) e.values_changed();
  }

  public function js_setup(form:JQuery) {
    for (x in o.elements) x.js_setup(form);
  }

  public function assignValuesFromDOM(){
    for (x in o.elements) x.assignValuesFromDOM();
  }

  public function jsUpdateErrorDisplay(){
    for (x in o.elements) x.jsUpdateErrorDisplay();
  }
#end

}
