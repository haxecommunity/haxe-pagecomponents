package pc.form;
import Cfg;

#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end
#if SERVER
import web.RequestResponseHelper;
#end

import pc.form.Content;
import pc.Form;
import haxe.macro.Expr;
import haxe.macro.Context;
using Reflect;
using Std;

enum InputType {
  text;
  password;
  file;
  select(s:Array<{key:String, value:String}>);
  datetime(secs:Bool);
  date(date_opts:{?year_range:{from:Int, to:Int}});
  time(secs:Bool);
  int(range:Null<{?from:Int, ?to:Int}>);
  textarea(o:Null<{?cols: Int, ?rows: Int}>);
  checkbox;
}

// represents a simple
typedef InputData = {
  name:String,
  label:String,
  ctx: Ctx,
  type: InputType,
  ?check: check.CheckFunction,
  ?html_tags: Dynamic, // { style="....", etc ... }
  ?default_: Dynamic,
  ?hint:String,
  ?hint_on_form: String
};

// represents a simple input field
class Input
  implements Content {

  public var o:InputData;

  public var form(default, set_form): Form<Dynamic>;
  public function set_form(f:Form<Dynamic>){
    this.form = f;
    return f;
  }

  public function values_changed(){ }

  public function getLabel(){ return o.label; };
  public function getName(){ return o.name; };

  public function new(o:InputData) {
    this.o = o;

    if (o.html_tags == null)
      o.html_tags = {}

      var extra_check = switch (o.type){
        case time(secs):
          check.CheckImplementations.time();
        case int(r):
         o.check = check.CheckImplementations.int_range(r);
        case _: null;
      };
      if (extra_check != null)
        o.check = check.CheckImplementations.combine(o.check, extra_check);
  }


  public function assignErrors(){
    if (o.check == null) return;

    var e = o.check(o.ctx.translate, form.values.field(o.name));
    if (e != null){
      form.errors.setField(o.name, e);
    }
  }

  public function html():String {
    var value = form.values.field(o.name);
    if (value == null) value = this.o.default_;

    var error =
      null == form.errors.field(o.name)
      ? {}
      : {error: form.errors.field(o.name) };

    var name_id = {name: this.o.name, id: this.o.name}; // id required for labels to work

    var html = switch (this.o.type) {
      case text:
        mw.HTMLTemplate.str('
          %input(name_id title=this.o.hint error this.o.html_tags value=value)
        ');
      case int(o):
        mw.HTMLTemplate.str('
          %input(name_id type="text" title=this.o.hint error this.o.html_tags value=value)
        ');

      case password:
        mw.HTMLTemplate.str('
          %input(name_id type="password" title=this.o.hint error this.o.html_tags value=value)
        ');

      case file:
        mw.HTMLTemplate.str('
          %input(name_id type="file" title=this.o.hint error this.o.html_tags value=value)
        ');

      case select(items):
        mw.HTMLTemplate.str('
          %select(name=this.o.name title=this.o.hint error this.o.html_tags)
            :for(i in items)
              :if (i.key == value)
                %option(value=i.key selected="selected")=i.value
              :else
                %option(value=i.key)=i.value
        ');
      case date(date_opts):
#if HAVE_JQUERY_UI

        if (value.is(Date))
          value = o.ctx.localization.date_to_str(value);
        // type date is not supported by all browsers, it may be lacking today, using jquery instead
        mw.HTMLTemplate.str('
          %input(name_id type="text" title=this.o.hint error this.o.html_tags value=value)
        ');
#end
      case datetime(secs):
        if (value.is(Date))
          value = o.ctx.localization.datetime_to_str(value, secs);

#if HAVE_JQUERY_UI
        // type date is not supported by all browsers, it may be lacking today, using jquery instead
        mw.HTMLTemplate.str('
          %input(name_id type="text" title=this.o.hint error this.o.html_tags value=value)
        ');
#end
      case time(secs):
        if (value.is(Date))
          value = o.ctx.localization.time_to_str(value, secs);

        // type date is not supported by all browsers, it may be lacking today, using jquery instead
        mw.HTMLTemplate.str('
          %input(type="text" name=this.o.name title=this.o.hint error this.o.html_tags value=value)
        ');

      case textarea(opts):
        var cols_rows = {
          rows: (opts == null || opts.rows == null) ? 10 : opts.rows
        };
        if (opts != null && opts.cols != null)
          cols_rows.setField('cols', opts.cols);

        mw.HTMLTemplate.str('
          %textarea(name_id title=this.o.hint error this.o.html_tags cols_rows)=value
        ');
      case checkbox:
        var checked = value ? {checked: "checked"} : {};
        mw.HTMLTemplate.str('
          %input(name_id type="checkbox" title=this.o.hint error this.o.html_tags checked)
        ');
    };

    var hint =
      (this.o.hint_on_form == null 
          ? ""
          : mw.HTMLTemplate.str('
            !=this.o.hint_on_form
            %br
          ')
        );
    return hint + html;
  }

#if SERVER
  public function assignValuesFromPost(avp:AVP){
    switch (this.o.type) {
      case int(_):
        form.values.setField(o.name, avp.post(o.name));
      case text:
        form.values.setField(o.name, avp.post(o.name));
      case password:
        form.values.setField(o.name, avp.post(o.name));
      case file:
        form.values.setField(o.name, avp.file(o.name));
      case select(items):
        form.values.setField(o.name, avp.post(o.name));
      case date(date_opts):
        form.values.setField(o.name, avp.post(o.name));
      case time(secs):
        form.values.setField(o.name, avp.post(o.name));
      case datetime(secs):
        form.values.setField(o.name, avp.post(o.name));
      case textarea(opts):
        form.values.setField(o.name, avp.post(o.name));
      case checkbox:
        var p = avp.post(o.name);
        var v = switch (p) {
          case "on": true;
          case "": false;
          case _: throw 'unexpected ${p}';
        }
        form.values.setField(o.name, v);
    }

  }
#end

#if JS_CLIENT

  public var el: JQuery;

  public function jsUpdateErrorDisplay() {
    if (form.errors.hasField(o.name)){
      this.el.attr("error", form.errors.field(o.name));
    } else {
      this.el.removeAttr("error");
    }
    assignErrorClass();
  }


  public function assignValuesFromDOM(){
    switch (this.o.type) {
      case checkbox:
        form.values.setField(o.name, el.is(':checked'));
      case _:
        form.values.setField(o.name, el.val());
      // case int(_):
      // case text:
      // case password:
      // case file:
      //   throw "unlikely to work"; // TODO
      // case select(items):
      // case datetime(_):
      // case time(_):
      // case date:
      // case textarea(opts):
      // case checkbox:
    }
  }

  public function js_setup(el_form:JQuery) {
    this.el = el_form.find('[name=${o.name}]');
    if (this.el == null)
      throw "bad";

    switch (this.o.type) {
      case date(date_opts):
#if HAVE_JQUERY_UI
        // type date is not supported by all browsers, it may be lacking today, using jquery instead
        var opts = {
          showButtonPanel: true, // for [ TODAY ] button
          changeMonth: true,
          changeYear: true
        };
        if (date_opts.year_range != null){
          opts.setField("yearRange", '${date_opts.year_range.from}:${date_opts.year_range.to}');
        }
        untyped el.datepicker(opts);
#end
      case _:
    }

    assignErrorClass();
    untyped this.el.tooltip();

    function changed(e){
      e.preventDefault();
      assignValuesFromDOM();
      form.values_changed();
    }

    this.el.change(changed);

  }

  public function assignErrorClass() {
    if (untyped this.el.attr("error")){
      this.el.addClass("error");
      this.el.attr('title', this.el.attr("error")+"<br/>"+o.hint);
    } else {
      this.el.removeClass("error");
      this.el.attr('title', this.o.hint);
    }
  }

#end
}


