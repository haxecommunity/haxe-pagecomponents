package pc.form;
#if !macro
import  web.RequestResponseHelper;
#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end

/* minimal thing which 
- can render itself (server & client implementation
- knows how to assign values to a anon structure
- knows how to assign errors
- a form content is meant to contain both: the layout and additional elements
*/

typedef VE = {values: {}, errors: {}};
#if SERVER
typedef AVP = {
  post:String -> Null<String>,
  file:String -> Null<File>
};
#end

interface Content {

  public var form(default, set_form): Form<Dynamic>;
  public function set_form(f:Form<Dynamic>):Form<Dynamic>;


  function getName():String;
  function getLabel():String;
  function html():String;

  // this function is responsible to turn a date str into a date object
  // (because only after checking its known that this conversion is valid)
  function assignErrors():Void; 

#if SERVER
  // assign values from post to values
  function assignValuesFromPost(avp: AVP): Void;
#end

#if js
  // if an element changes the value it should notify the form by assigning form.values.field and calling values_changed
  function js_setup(el_form:JQuery): Void;
  function assignValuesFromDOM():Void; // use form.el_form to get values
  function jsUpdateErrorDisplay():Void;
  function values_changed():Void;
#end
}
#end
