package pc;
import Cfg;

#if JS_CLIENT
import js.html.Element;
import jQuery.*;
#end

using Reflect;
using StringTools;
using Type;
using web.RequestResponseHelper;
using mw.DeepCopy;
using mw.ArrayExtensions;
using mw.NullExtensions;

import GoogleAnalyticsEvent;

// ga_calls = [["_trackPageview", "angemeldet"]]
enum ActionResult {
  ar_serve_file;
  ar_js(js:String);
  ar_replace_html_of_dom_el(data:{id:String, html:String, ?google_analytics_events:Array<GoogleAnalyticsEvent>});
  ar_open_url(url:String);
  ar_reload_page;
}

// throw a message the user should see
class ActionError {
  public var msg:String;
  public function new(msg:String) {
    this.msg = msg;
  }
}

/* a page component has its own id, can reload itself and the like
 */

typedef PCData = {
  ?id:String,
  ctx:Ctx,
}
class PC<O:PCData> {
  public var o:O;

  public var js_instance: Null<String>; // if null don't create js instance, otherwise this string serves as input to js_instance_args unless it gets overridden
  public var classname:String;

#if JS_CLIENT
  public var el:JQuery;
  public function js_setup(el:JQuery): Void {
    this.el = el;
  }
  public function resize() {
    // you can override this if you want to resize your elements
  }
#end

  static public function new_id() {
    var r = haxe.crypto.Md5.encode(Math.random()+"");
    var id = 'id${r}';
    return id;
  }

  public function new(o) {
    this.o  = o;
    this.classname = Type.getClassName(Type.getClass(this));
    if (null == o.id){
      this.o.id = new_id();
    }
#if JS_CLIENT
    if (this.o.id == null && this.el != null)
      this.o.id = this.el.attr("id");
#end
  }

  public function innerHTML():String {
    return "TODO"; // override in subclass
  }


  public function haxe_instance_args_impl(pass:String = "id"):Array<Dynamic> {
    // assume first argument of type this.o:
    return switch (pass) {
      case "": [];
      case "id": [{id: o.id}];
      case "all": 
        // var o_copy = {};
        // for (f_name in o.fields()){
        //   var f = o.field(f_name);
        //   if (!Reflect.isFunction(f))
        //     o_copy.setField(f_name, f);
        // }
         [o]; // TODO: remove unserializable stuff such as functions, they will be passed as string right now
      case _:
         var o_copy = {};
         for (f_name in pass.split(",")){
           o_copy.setField(f_name, o.field(f_name));
         }
         throw "unexpected"; // maybe interpret as , separated list of field names?
    }
  }

  public function haxe_instance_args():Array<Dynamic> {
    return haxe_instance_args_impl(js_instance);
  }

  public function html() {
    var opt_haxe = this.js_instance == null ? {} : {haxe: haxe.Serializer.run({class_: this.classname, args: haxe_instance_args()}) };
    opt_haxe.setField("class", this.getClass().getClassName().replace(".","_"));
    return mw.HTMLTemplate.str("
      %div(id=this.o.id opt_haxe)!=innerHTML()
    ");
  }

  public function actionURL(action:String, data:Dynamic) {
    var class_state = this.o.deepCopy();
    class_state.deleteField('ctx');
    #if JS_CLIENT
    if (class_state.hasField('el'))
      class_state.deleteField('el');
    #end

    haxe.Serializer.run(data).urlEncode();
    var data = haxe.Serializer.run(data).urlEncode();
    return this.o.ctx.http_uri_prefix
      +'/pc'
      + '/'+ this.classname
      + '/'+ action
      + '/'+ haxe.Serializer.run(class_state).urlEncode()
      + '/'+ haxe.Serializer.run(data).urlEncode();
  }

#if SERVER
  static public function handleAction(ctx:Ctx, url,
      f:Void -> Void // to be called before action is run
  ) {
    var reg = ~/pc\/([^\/]*)\/([^\/]*)\/([^\/]*)\/([^\/]*)/;
    if (reg.match(url)){
      var classname = reg.matched(1);
      var action =    reg.matched(2);
      var class_state = haxe.Unserializer.run(reg.matched(3).urlDecode());
      var data = haxe.Unserializer.run(reg.matched(4).urlDecode());

      if (!Cfg.pc_classes.contains(classname))
        throw 'bad classname ${classname}';

      var clazz = Type.resolveClass(classname);
      if (clazz == null)
        throw 'bad class ${classname}';

      Reflect.setField(class_state, 'ctx', ctx);
      var i = Type.createInstance(clazz, [class_state]);

      f();
      var r:ActionResult = Reflect.callMethod(i, Reflect.field(i, 'action_${action}') , [data]);
      if (r == null)
        throw "action didn't return valid result";

      function js_maybe_iframe(ctx:Ctx, js:String):String{
        if (ctx.getOrNull("iframe") == null){
          throw "unexpeced, no iframe get parameter ? TODO";
        } else {
          var js = 'parent.window.jQuery.globalEval('+haxe.Json.stringify(js)+');';
          return mw.HTMLTemplate.str('
            %html
              %head
              %body
                %script(language="javascript" type="text/javascript")
                  !=js
          ');
        }
      }

      switch (r) {
        case ar_serve_file:
          throw "TODO";

        case ar_js(js):
          ctx.end(200, js_maybe_iframe(ctx, js));
          return true;

        case ar_replace_html_of_dom_el(data):
          var ga_js = [];

          for(event in data.google_analytics_events.ifNull([])){
            switch (event) {
              case page_view(url):
                if (url.substr(0,1) != "/") throw "guard";
                ga_js.push('ga("send", "pageview", ${haxe.Json.stringify(url)});');
            }
          }

          // Enhancing the parent "enhances" too much, but html could be multiple elements
          // this still looks reasnoable to me
          ctx.end(200, js_maybe_iframe(ctx, '
            (function(){
                var el = $("#"+${haxe.Json.stringify(data.id)});
                var parent = el.parent();
                $(el.html(${haxe.Json.stringify(data.html)}).get(0).firstChild).unwrap();
                JSMain.enhance(parent.get(0));

                ${ga_js.join("\n")}
            })()
          '));
          return true;

        case ar_reload_page:
          ctx.end(200, js_maybe_iframe(ctx, 'window.location.reload()'));
          return true;

        case ar_open_url(url):
          ctx.end(200, js_maybe_iframe(ctx, 'window.location.href = ${haxe.Json.stringify(url)}'));
          return true;

        case _: throw 'to be implemented ${r}';
      }

    }
    return false;
  }
#end

}
