package check;
using mw.StringExtensions;

class CheckImplementations {

  static public function combine(f1: CheckFunction, f2:CheckFunction):CheckFunction {
	if (f2 == null) return f1;
	if (f1 == null) return f2;
	return function(t, v){
	  var s = f1(t,v);
	  if (s == null)
		s = f2(t,v);
	  return s;
	};
  }

  static public function non_empty(expected:String): CheckFunction {
    return function(t, v){
      if (v == null || v == "")
        return t("expected: _EXPECTED","").replaceAnon({_EXPECTED: expected});
      return null;
    }
  }

  static public function is_int():CheckFunction {
    return function(t, v){
      return (!~/^-?[0-9]+$/.match(v)) ? null : t("whole number expected", "");
    };
  }

  static public function int_range(o:{?from:Int, ?to:Int} = null):CheckFunction {
    return function(t,v){
      var r = is_int()(t,v);
      if (r == null){
        var i = Std.parseInt(v);
		var f = o == null ? null : o.from;
		var t_ = o == null ? null : o.to;

        if (f  != null && v < f) return t("whole number between A_ and B_ expected", "").replaceAnon({A_: f, B_: t_});
        if (t_ != null && v > t_) return t("whole number between A_ and B_ expected", "").replaceAnon({A_: f, B_: t_});
      }
      return r;
    }
  }

  static public function today_or_future():CheckFunction {
    return function (t,v){
      return null;
    }
  }

  public static var regex_email = ~/^[^@]+@[^.]+\..*$/;
  static public function email(): CheckFunction {
    return function (t,v){
      if (regex_email.match(v))
        return null;
      else return t("email expected","");
    }
  }

  static public function password(min_length:Int): CheckFunction {
    return function (t,v){
      if (v.length < min_length)
        return t("your password should have at least _NUM characters".replaceAnon({_NUM: min_length}),"");
      return null;
    }
  }

  static public function str_length(a:Null<Int>, b:Null<Int>): CheckFunction {
    return function(t,v){
      var l = v.length;
      if (a != null && l < a) return t('Between _A and _B characters expected', "").replaceAnon({_A: a, _B: b});
      if (b != null && l > b) return t("Between _A and _B characters expected", "").replaceAnon({_A: a, _B: b});
      return null;
    }
  }

  static public function mandatory(): CheckFunction {
    return function (t,v){
      if (v.length == 0)
        return t("mandatory field","");
      return null;
    }
  }

  public static var regex_time = ~/[0-9][0-9]:[0-9][0-9](:[0-9][0-9])?/;
  static public function time(): CheckFunction {
    return function (t,v){
      if (regex_time.match(v))
        return null
      else return t("time expected, format HH:MM(:SS)", "");
    }
  }

  static public function url(protocols:Array<String> = null): CheckFunction {
    if (protocols == null)
      protocols = ['http','https'];

    return function (t,v){
      return null;
    }
  }
}
