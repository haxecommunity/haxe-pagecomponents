haxe page components is a small library which allows to define HTML elements once
and reuse the code on server/client side.

The JS instances get created automatically by iterating over the elements finding
those having a special haxe tag which contains the initialization data.

TODO: think about adopting getbootstrap for fluid layouts
